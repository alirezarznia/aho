#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
vector<string>vec;
ll go[150001][52];
ll fail[150001];
ll out[150001];
bool ans[1001];
ll CO(){
    ll state= 0;
    Set(fail ,-1);
    Set(out ,0);
    Set(go , -1);
    Rep(i , vec.size()){
        ll current = 0;
        Rep(j , vec[i].size()){
            ll word = vec[i][j]-'a';
            if(go[current][word] == -1) go[current][word]=++state;
            current = go[current][word];
        }
        out[current]|=1<<i;
    }
    queue <ll> q;
    Rep(i , 52){
        if(go[0][i] ==-1) go[0][i]=0;
        else{
            fail[go[0][i]] = 0;
            q.push(go[0][i]);
        }
    }
    while(!q.empty()){
        ll st = q.front() ;q.pop();
        Rep(i , 52){
            if(go[st][i] != -1){
                ll ff =fail[st];
                while(go[ff][i] == -1 ) ff = fail[ff];
                ff = go[ff][i];
                fail[go[st][i]] = ff;
                out[go[st][i]] |= out[ff];
                q.push(go[st][i]);
            }
        }
    }
    return state;
}
string str; ll m ;
ll findcurrent(ll cur, char ch){
    ll num = ch-'a';
    while(go[cur][num] == -1) cur=fail[cur];
    return go[cur][num];
}
ll AHO(){
    CO();
    int current =0;
    Rep(i , str.size()){
        current = findcurrent(current , str[i]);
        if(out[current] == 0) continue;
        Rep(j , m){
            if(1<<j & out[current]) ans[j]=true;
        }
    }
    return 0;
}
int main(){
   // Test;
        Set(ans,false);
    vec.clear();
   cin >>str;
    cin >>m;
    Rep(i ,m ){
        string ss ; cin >> ss ; vec.push_back(ss);
    }
     AHO();
     Rep(i ,m){ if(ans[i]) printf("Y\n");
            else printf("N\n");
        }
    return 0;
}